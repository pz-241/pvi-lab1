// Add new row
document.getElementById('add_btn').onclick = function(){
    const newTableElement = document.createElement('tr');

    newTableElement.innerHTML = `
        <th><input type="checkbox"></th>
        <td class="group">IR-25</td>
        <td>Sobran Oleksandr</td>
        <td>M</td>
        <td>23.10.2004</td>
        <td>
        <span class="status-dot"></span>
        </td>
        <td>
            <button class="btn-edit">
            <i class="fa-regular fa-pen-to-square fa-xl"></i>
            </button>
            <button class="btn-delete">
            <i class="fa-solid fa-trash fa-xl"></i>
        </button>
        </td>
    `;

    newTableElement.classList.add('table_row');
    document.getElementById('table_body').appendChild(newTableElement);

    const newDeleteButton = newTableElement.querySelector('.btn-delete');
    addDeleteListener(newDeleteButton);
}

// Delete row
let deleteButtons = document.querySelectorAll('.btn-delete');
deleteButtons.forEach(button => addDeleteListener(button));

function addDeleteListener(button) {
    button.addEventListener('click', function() {
        const row = button.closest('tr');
        row.remove();
    });
}